package com.example.android.fawqa_1202160101_si4005_pab_modul1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    EditText editText1;
    EditText editText2;
    TextView textView;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText1 = (EditText) findViewById(R.id.Angka1);
        editText2 = (EditText) findViewById(R.id.Angka2);
        button = (Button) findViewById(R.id.buttoncek);
        textView = (TextView) findViewById(R.id.hasil);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            hitung();

            }
        });

    }
    public void hitung(){
        Float nilai1 = Float.parseFloat(editText1.getText().toString());
        Float nilai2 = Float.parseFloat(editText2.getText().toString());
        Float hitungnilai = nilai1*nilai2;
        textView.setText(hitungnilai.toString());
    }
}
